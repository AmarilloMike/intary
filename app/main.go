package main

import (
	"MikeAustin71/intary/common"
	"fmt"
	"time"
	"math/big"
	"strconv"
)



func main() {

	//ia1, _ := common.IntAry{}.NewNumStr("-1245.25787")

	num := float64(350.325)
	precision := int(2)

	numStr :=  strconv.FormatFloat(float64(num), 'f', -1, 64)

	fmt.Println("num=", num)
	fmt.Println("precision=", precision)
	fmt.Println("numStr= ",numStr)
	num2 := float32(-350.325)
	numStr2 := strconv.FormatFloat(float64(num2), 'f', -1, 64)

	fmt.Println("num=", num2)
	fmt.Println("precision=", precision)
	fmt.Println("numStr= ",numStr2)

	/*
	outPrecision := 5

	err := ia1.AddFloat32ToThis(num, precision)

	expected := "-85595.58187"

	if err != nil {
		fmt.Printf("Error returned from ia1.AddFloat32ToThis(num, precision). num='%v' precision='%v' err='%v'\n", num, precision, err)
		return
	}

	if outPrecision != ia1.GetPrecision() {
		fmt.Printf("Expected output precision='%v'. Instead ia1.GetPrecision()= '%v'", outPrecision, ia1.GetPrecision())
		return
	}


	result := ia1.GetNumStr()

	if expected != result {
		fmt.Printf("Expected result='%v'. Instead, result='%v' ",expected, result)
		return
	}


	fmt.Println("SUCCESS!!!!")
	*/
}

func PowerByTwo(number string, power *big.Int, maxResultPrecision, internalPrecision int, expected string) {

	// 97.3^7 expected := "82563839680943.6069797"
	//  4.3789^-8 expected := "0.0000073974115435604814873569688138682329855849" 46-digits

	// 2^7 expected := "128"
	// 3^17 expected := "129140163"
	// 4^20 expected := "1099511627776"
	// 5^20 expected := "95367431640625"

	fh := common.FileHelper{}
  fileName:= "D:/go/work/src/MikeAustin71/intary/app/resultOutput.txt"
	if fh.DoesFileExist(fileName) {
		fh.DeleteDirFile(fileName)
	}


	fmt.Printf("      number: %v\n", number)
	fmt.Printf("       power: %v\n", power)
	fmt.Printf("    expected: %v\n", expected)
	ia, _ := common.IntAry{}.NewNumStr(number)
	startTime := time.Now()
	err := ia.PowByTwos(power, maxResultPrecision, internalPrecision)
	endTime := time.Now()

	if err != nil {
		fmt.Printf("Error returned from ia.PowByTwos(power, maxResultPrecision, internalPrecision). Error= %v", err)
		return
	}

	du := common.DurationUtility{}
	du.SetStartEndTimes(startTime, endTime)

	if err != nil {
		fmt.Printf("Error returned from ia.PowByTwos(power, maxResultPrecision). Error= %v \n", err)
	}

	resultStr := ia.GetNumStr()

	fmt.Printf("      result: %v\n", resultStr)


	fmt.Printf("maxResultPrecision: %v\n", maxResultPrecision)
	fmt.Printf("         Precision: %v\n", ia.GetPrecision())
	fmt.Println("---------------------------------------")
	display, _ := du.GetHoursTime()
	fmt.Println("Elapsed Time: ", display.DisplayStr)
	fmt.Println("=================================")

	f, err := fh.CreateFile(fileName)

	if err != nil {
		fmt.Printf("Error returned from fh.CreateFile(fileName). Error= %v", err)
		return
	}
	defer f.Close()

	fh.WriteFileStr(resultStr, f)

}


func GetString(ia1 common.IntAry, expected string) {

	fmt.Println("-----------------------------------")
	fmt.Println("          GetString()              ")
	resultNumStr := ia1.GetNumStr()
	fmt.Println("   GetNumStr: ", resultNumStr)
	fmt.Println("    expected: ", expected)
	if expected == resultNumStr {
		fmt.Println("SUCCESS!!!! - Expected is EQUAL TO result")
	} else {
		fmt.Println("FAILURE**** - Expected is NOT EQUAL TO result")
	}

}

func MultiplyInternal(ia1, ia2, result *common.IntAry, maxPrecision int, expected string) {

	fmt.Println("-----------------------------------")
	fmt.Println("             Multiply              ")
	multiplier := ia1.GetNumStr()
	iaAdmin := common.IntAry{}
	startTime := time.Now()
	iaAdmin.Multiply(ia1, ia2, result, maxPrecision)
	endTime := time.Now()
	du := common.DurationUtility{}
	du.SetStartEndTimes(startTime, endTime)
	resultNumStr := result.GetNumStr()
	fmt.Println("  multiplier: ", multiplier)
	fmt.Println("multiplicand: ", ia2.GetNumStr())
	fmt.Println("      result: ", resultNumStr)
	fmt.Println("    expected: ", expected)
	fmt.Println("-----------------------------------")
	fmt.Println("-----------------------------------")
	if expected == resultNumStr {
		fmt.Println("SUCCESS!!!! - Expected is EQUAL TO result")
	} else {
		fmt.Println("FAILURE**** - Expected is NOT EQUAL TO result")
	}
	fmt.Println("---------------------------------------")
	display, _ := du.GetHoursTime()
	fmt.Println("Elapsed Time: ", display.DisplayStr)
	fmt.Println("=================================")

}


func MultiplyThisBy(this, ia2 *common.IntAry, expected string) {

	fmt.Println("-----------------------------------")
	fmt.Println("    Multiply This By        ")
	multiplier := this.CopyOut()
	startTime := time.Now()
	this.MultiplyThisBy(ia2, -1)
	endTime := time.Now()
	du := common.DurationUtility{}
	du.SetStartEndTimes(startTime, endTime)
	resultNumStr := this.GetNumStr()
	fmt.Println("  multiplier: ", multiplier.GetNumStr())
	fmt.Println("multiplicand: ", ia2.GetNumStr())
	fmt.Println("      result: ", resultNumStr)
	fmt.Println("    expected: ", expected)
	fmt.Println("-----------------------------------")
	fmt.Println("-----------------------------------")
	if expected == resultNumStr {
		fmt.Println("SUCCESS!!!! - Expected is EQUAL TO result")
	} else {
		fmt.Println("FAILURE**** - Expected is NOT EQUAL TO result")
	}
	fmt.Println("---------------------------------------")
	display, _ := du.GetHoursTime()
	fmt.Println("Elapsed Time: ", display.DisplayStr)
	fmt.Println("=================================")



}


func ThisSquared() {
	nStr1 := "5.2"
	expected := "27.04"
	eSignVal := 1
	ePrecision := 2
	ia, _ := common.IntAry{}.NewNumStr(nStr1)

	err := ia.MultiplyThisBy(&ia, -1)

	if err != nil {
		fmt.Printf("Error returned from ia.MultiplyThisBy(&ia, true). Error= %v \n", err)
	}

	fmt.Printf("  number: %v\n", nStr1)
	fmt.Printf("expected: %v\n", expected)
	fmt.Printf("  result: %v\n", ia.GetNumStr())

	if expected != ia.GetNumStr() {
		fmt.Printf("Expected ia.GetNumStr() == %v . Instead ia.GetNumStr() == %v /n", expected, ia.GetNumStr())
	}

	if eSignVal != ia.GetSign() {
		fmt.Printf("Expected ia.GetSign() == %v . Instead ia.GetSign() == %v \n", eSignVal, ia.GetSign())
	}

	if ePrecision != ia.GetPrecision() {
		fmt.Printf("Expected ia.GetPrecision() == %v . Instead ia.GetPrecision() == %v \n", ePrecision, ia.GetPrecision())
	}

}



func fracTest() {

	numStrNum := "3.1"
	numStrDenom := "4.2"
	eFrac := "0.738095238095"
	precision := 12
	fIary, _ := common.FracIntAry{}.NewNumStrs(numStrNum, numStrDenom)

	ratNum, err := fIary.GetRationalValue(-1)

	if err != nil {
		fmt.Printf("Error returned from fIary.GetRationalValue() - Error= %v \n", err)
	}

	fmt.Println("      Numerator: ", numStrNum)
	fmt.Println("    Denominator: ", numStrDenom)
	fmt.Println(" Expected Float: ", eFrac)
	fmt.Println("      Rat Float: ", ratNum.FloatString(precision))
	fmt.Println("  Rat Numerator: ", ratNum.Num())
	fmt.Println("Rat Denominator: ", ratNum.Denom())

}

func DivideTest() {
	dividend := "1"
	divisor := "5132188731375616"
	eQuotient := "0.00000000000000019484864106545884795863650193677353"
	eSignVal := 1
	maxPrecision := 1024
	ePrecision := 1024

	ia1, _ := common.IntAry{}.NewNumStr(dividend)

	ia2, _ := common.IntAry{}.NewNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		fmt.Printf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v \n", err)
	}

	if eQuotient != quotient.GetNumStr() {
		fmt.Printf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .\n", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.GetPrecision() {
		fmt.Printf("Expected quotient.GetPrecision()= '%v' .  Instead, quotient.GetPrecision()= '%v'  .\n", ePrecision, quotient.GetPrecision())
	}

	if eSignVal != quotient.GetSign() {
		fmt.Printf("Error - Expected smop.Quotient.GetSign()= '%v'. Instead, smop.Quotient.GetSign()= '%v' .\n", eSignVal, quotient.GetSign())
	}

	fmt.Println("         divisor: ", divisor)
	fmt.Println("        dividend: ", dividend)
	fmt.Println("       eQuotient: ", eQuotient)
	fmt.Println(" actual quotient: ", quotient.GetNumStr())
	fmt.Println("actual precision: ", quotient.GetPrecision())

}


func PowerTest() {
	nStr1 := "92"
	power := -8
	eStr := "1.9484864106545884795863650193677"
	eSignVal := 1
	maxPrecision:= 350
	ePrecision := 6

	fmt.Printf("  number: ", nStr1)
	fmt.Printf("   power: ", power)
	fmt.Printf("expected: ", eStr)
	ia := common.IntAry{}.New()

	ia.SetIntAryWithNumStr(nStr1)

	err := ia.Pow(power, maxPrecision, -1)

	if err != nil {
		fmt.Printf("Error returned from ia.Pow(power). Error= %v \n", err)
	}

	if eStr != ia.GetNumStr() {
		fmt.Printf("Expected ia.GetNumStr() == %v . Instead ia.GetNumStr() == %v /n", eStr, ia.GetNumStr())
	}

	if eSignVal != ia.GetSign() {
		fmt.Printf("Expected ia.GetSign() == %v . Instead ia.GetSign() == %v \n", eSignVal, ia.GetSign())
	}

	if ePrecision != ia.GetPrecision() {
		fmt.Printf("Expected ia.GetSign() == %v . Instead ia.GetSign() == %v \n", eSignVal, ia.GetSign())
	}

}
