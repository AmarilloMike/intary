# intary
This Golang utility used in managing integer arrays and performing math operations on those integer arrays.

The primary library is located in the source file ./common/intary.go.

'intary.go' contains a structure named, 'IntAry'. This structure and
provides methods for performing a variety of math operations on 'IntAry'
objects. 'IntAry' objects are capable of performing complex math operations
on very large numbers.

The directory ./archives/eulersnumbercalc contains an example which calculates
Euler's Number out to 1,000 digits.


### Source Code Repository
The source code repository for this library is located at Bitbucket:

https://bitbucket.org/AmarilloMike/intary/src


### Dependencies

The 'IntAry' object has the following dependency:

nthroot.go - Source Repository:

https://bitbucket.org/AmarilloMike/mathhlpr/src

